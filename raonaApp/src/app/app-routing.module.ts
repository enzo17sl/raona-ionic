import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'users-list',
    loadChildren: () => import('./pages/users/users-list/users-list.module').then( m => m.UsersListPageModule)
  },
  {
    path: 'users-detail/:id',
    loadChildren: () => import('./pages/users/users-detail/users-detail.module').then( m => m.UsersDetailPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
