import { UsersService } from '../../../modules/providers/users/users.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
})
export class UsersListPage implements OnInit, OnDestroy {
  users: User[] = [];
  progress: boolean;
  private subscriptionService: Subscription;
  constructor(
    private usersService: UsersService
  ) { }
  ngOnDestroy(): void {
    this.onUnSubscribeService();
  }

  ngOnInit() {
    this.getUsersList();
  }

  async getUsersList() {
    this.progress = true;
    this.subscriptionService = this.usersService.getUsersList().subscribe(res => {
      this.users = res;
      this.progress = false;
    }, err => {
      this.progress = false;
      console.log(err);
    });
  }

  private onUnSubscribeService() {
    if (this.subscriptionService) {
      this.subscriptionService.unsubscribe();
    }
  }
}
