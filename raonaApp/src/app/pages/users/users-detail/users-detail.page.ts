import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/modules/providers/users/users.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.page.html',
})
export class UsersDetailPage implements OnInit, OnDestroy {
  userId = null;
  currentUser: User;
  progress: boolean;
  private subscriptionService: Subscription;
  constructor(
    private activeRoute: ActivatedRoute,
    private usersService: UsersService) {
    this.userId = this.activeRoute.snapshot.paramMap.get('id');
  }
  ngOnDestroy(): void {
    this.onUnSubscribeService();
  }

  ngOnInit() {
    this.getUserData();
  }

  async getUserData() {
    this.progress = true;
    this.subscriptionService = this.usersService.getUserDetail(this.userId).subscribe(res => {
      this.currentUser = res;
      this.progress = false;
    }, err => {
      this.progress = false;
      console.log(err);
    });
  }

  private onUnSubscribeService() {
    if (this.subscriptionService) {
      this.subscriptionService.unsubscribe();
    }
  }
}
