import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
})
export class ToolbarComponent implements OnInit {
  @Input() backButton = false;
  currentIcon = '';
  constructor(
    private nav: NavController) {
    this.currentIcon = environment.userIcon;
  }

  ngOnInit() { }
  onBack() {
    this.nav.back();
  }
}
