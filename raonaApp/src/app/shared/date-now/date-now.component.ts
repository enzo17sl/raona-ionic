import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-date-now',
  templateUrl: './date-now.component.html',
})
export class DateNowComponent implements OnInit {
  dateNow: string;
  remainingDays: number;
  constructor() { }

  ngOnInit() {
    this.dateNow = moment(moment().toDate()).format('DD/MM/YYYY');
    this.remainingDays = moment().daysInMonth() - moment().toDate().getDate();
  }

}
