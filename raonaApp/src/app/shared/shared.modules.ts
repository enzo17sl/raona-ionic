import { SpinnerComponent } from './spinner/spinner.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { DateNowComponent } from './date-now/date-now.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        DateNowComponent,
        ToolbarComponent,
        SpinnerComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
        DateNowComponent,
        ToolbarComponent,
        SpinnerComponent
    ],
    providers: [
    ]
  })
  export class SharedModule { }
