import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/interfaces/user';
@Injectable({
    providedIn: 'root'
  })
  export class UsersService {
    constructor(private http: HttpClient) { }

    getUsersList() {
        return this.http.get<User[]>(`${environment.apiServiceURL}/users`);
    }

    getUserDetail(id: string){
    return this.http.get<User>(`${environment.apiServiceURL}/users/` + id);
    }
  }
