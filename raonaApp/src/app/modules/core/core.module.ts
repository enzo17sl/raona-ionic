import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './interceptor/jwt.interceptor';
import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/compiler/src/core';
@NgModule({
    imports: [
    ],
    exports: [
    ]
})

export class CoreModule {
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: CoreModule,
        providers: [ { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        ],
      } as ModuleWithProviders as ModuleWithProviders;
    }
  }
