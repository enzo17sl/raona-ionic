import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-users-detail-presentation',
  templateUrl: './users-detail-presentation.component.html',
  styleUrls: ['./users-detail-presentation.component.scss']
})
export class UsersDetailPresentationComponent implements OnInit {
  @Input() currentData: string;
  constructor() { }

  ngOnInit() {}

}
