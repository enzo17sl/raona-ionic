import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-users-list-item',
  templateUrl: './users-list-item.component.html',
})
export class UsersListItemComponent implements OnInit {
  @Input() currentUser: User;
  constructor(
    private nav: NavController) { }

  ngOnInit() {}
  onShowUsersDetail(){
    this.nav.navigateForward('/users-detail/' + this.currentUser.id);
  }
}
