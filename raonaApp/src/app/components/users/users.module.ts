import { UsersDetailImageComponent } from './users-detail-image/users-detail-image.component';
import { UsersDetailPresentationComponent } from './users-detail-presentation/users-detail-presentation.component';
import { UsersDetailDataComponent } from './users-detail-data/users-detail-data.component';
import { UsersListItemComponent } from './users-list-item/users-list-item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
      UsersListItemComponent,
      UsersDetailDataComponent,
      UsersDetailPresentationComponent,
      UsersDetailImageComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
      UsersListItemComponent,
      UsersDetailDataComponent,
      UsersDetailPresentationComponent,
      UsersDetailImageComponent
    ],
    providers: [
    ]
  })
  export class UserModule { }
