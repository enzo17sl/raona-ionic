import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-users-detail-data',
  templateUrl: './users-detail-data.component.html',
})
export class UsersDetailDataComponent implements OnInit {
  @Input() currentUser: User;
  constructor() { }

  ngOnInit() {}

}
