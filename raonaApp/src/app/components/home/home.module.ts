import { HomeBtnUsersComponent } from './home-btn-users/home-btn-users.component';
import { HomeLogoComponent } from './home-logo/home-logo.component';
import { HomeTitleComponent } from './home-title/home-title.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        HomeTitleComponent,
        HomeLogoComponent,
        HomeBtnUsersComponent
    ],
    imports: [
      CommonModule,
      IonicModule,
    ],
    exports: [
        HomeTitleComponent,
        HomeLogoComponent,
        HomeBtnUsersComponent
    ],
    providers: [
    ]
  })
  export class HomeModule { }
