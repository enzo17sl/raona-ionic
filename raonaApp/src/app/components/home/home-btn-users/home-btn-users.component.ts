import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-btn-users',
  templateUrl: './home-btn-users.component.html',
})
export class HomeBtnUsersComponent implements OnInit {

  constructor(private nav: NavController) { }

  ngOnInit() {}
  onShowUsersList(){
    this.nav.navigateForward('/users-list');
  }
}
