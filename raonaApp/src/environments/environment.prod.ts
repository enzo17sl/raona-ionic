export const environment = {
  production: true,
  apiServiceURL: 'https://jsonplaceholder.typicode.com',
  userIcon: 'assets/avatar/avatar.svg',
};
