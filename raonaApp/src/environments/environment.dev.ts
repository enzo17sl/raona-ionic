export const environment = {
    production: false,
    apiServiceURL: 'https://jsonplaceholder.typicode.com',
    userIcon: 'assets/avatar/avatar-dev.svg',
  };
